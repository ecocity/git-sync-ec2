#!/bin/sh

instance_id=$(git config --get sync-ec2.instance-id)

echo `aws ec2 describe-instances --instance-ids $instance_id | jq ".Reservations[0].Instances[0].State.Name" | sed "s/^\([\"']\)\(.*\)\1\$/\2/g"`
exit 0;
