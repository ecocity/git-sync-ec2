#!/bin/sh

status=$(git-sync-ec2-instance-status.sh)

if [[ "$status" != "stopped" && "$status" != "pending" && "$status" != "running" && "$status" != "stopping" ]]
then
	echo "Unknown status of EC2 instance; exitting";
	exit 1;
fi

instance_id=$(git config --get sync-ec2.instance-id)

while [[ "$status" != "running" ]]
do
	echo "Starting..."
    temp=$(aws ec2 start-instances --instance-ids $instance_id)
    sleep 10s;
    status=$(git-sync-ec2-instance-status.sh)
done

name=$(git-sync-ec2-instance-name.sh)

echo "Started as $name."
