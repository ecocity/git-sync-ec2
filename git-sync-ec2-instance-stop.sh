#!/bin/sh

status=$(git-sync-ec2-instance-status.sh)

if [[ "$status" != "stopped" && "$status" != "pending" && "$status" != "running" ]]
then
	echo "Unknown status of EC2 instance; exitting";
	exit 1;
fi

name=$(git-sync-ec2-instance-name.sh)
echo "Stopping $name"

while [[ "$status" != "stopped" ]]
do
	echo "Stopping..."
    temp=$(aws ec2 stop-instances --instance-ids i-94bf80a1);
    sleep 10s;
    status=$(git-sync-ec2-instance-status.sh)
done

echo "Stopped."
